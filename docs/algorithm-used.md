# 적용된 알고리즘

> 전체 코드는 [여기](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/complete_application/src/src.zip)에서 다운로드받을 수 있다.

어플리케이션에는 데이터 정리, 기준 예측 생성, 예측을 위한 머신 러닝(ML) 활용, 계산 메트릭, 예측 표시를 위한 데이터세트 생성 등 다양한 작업을 위한 기능이 포함되어 있다.

`clean_data` 함수는 초기 데이터세트를 정리하는 작업을 관리한다. 이는 `Date` 열을 datetime 형식으로 변환하여 이루어진다. 이 함수는 초기 DataFrame을 입력으로 받아 해당 DataFrame의 정리된 복사본을 출력으로 전달한다.

```python
def clean_data(initial_dataset: pd.DataFrame):
    print("     Cleaning data")
    initial_dataset['Date'] = pd.to_datetime(initial_dataset['Date'])
    cleaned_dataset = initial_dataset.copy()
    return cleaned_dataset
```

## 예측:
`predict_baseline()`과 `predict_ml()`은 정리된 DataFrame(`cleaned_dataset`)에서 예측 값, 수행할 예측 수(`n_predictions`), 특정 날짜(`day`) 및 최대 용량 값(`max_capacity`)을 반환한다.

처음에는 지정된 날짜까지 훈련 데이터세트를 선택한다. 그 후 예측을 생성하기 위해 특정 계산이나 조정을 수행한다. 이러한 예측이 최대 한계를 초과하지 않도록 하는 것이 중요하다.

```python
def predict_baseline(cleaned_dataset: pd.DataFrame, n_predictions: int, day: dt.datetime, max_capacity: int):
    print("     Predicting baseline")
    train_dataset = cleaned_dataset[cleaned_dataset['Date'] < day]

    predictions = train_dataset['Value'][-n_predictions:].reset_index(drop=True)
    predictions = predictions.apply(lambda x: min(x, max_capacity))
    return predictions

# This is the function that will be used by the task
def predict_ml(cleaned_dataset: pd.DataFrame, n_predictions: int, day: dt.datetime, max_capacity: int):
    print("     Predicting with ML")
    # Select the train data
    train_dataset = cleaned_dataset[cleaned_dataset["Date"] < day]

    # Fit the AutoRegressive model
    model = AutoReg(train_dataset["Value"], lags=7).fit()

    # Get the n_predictions forecasts
    predictions = model.forecast(n_predictions).reset_index(drop=True)
    predictions = predictions.apply(lambda x: min(x, max_capacity))
    return predictions
```

`compute_metrics()`는 예측에 대한 일부 메트릭을 계산한다.

```python
def compute_metrics(historical_data, predicted_data):
    historical_to_compare = historical_data[-len(predicted_data):]['Value']
    rmse = mean_squared_error(historical_to_compare, predicted_data)
    mae = mean_absolute_error(historical_to_compare, predicted_data)
    return rmse, mae
```

## 출력 데이터세트
`create_predictions_dataset()`은 시각화 목적으로 예측 데이터 세트를 만든다. 다음과 같은 작업이 필요하다.

- 예측된 기준 값(`predict_baseline`),
- ML 예측값(`predictions_ml`),
- 특정 날짜(`day`), 예측할 수(`n_predictions`),
- 정리된 데이터셋(`cleaned_data`).

이 함수는 날짜, 기록 값, ML 예측 값 및 기준 예측 값이 포함된 DataFrame을 반환한다.

```python
def create_predictions_dataset(predictions_baseline, predictions_ml, day, n_predictions, cleaned_data):
    print("Creating predictions dataset...")

    ...

    predictions_dataset = pd.concat([
        historical_data["Date"],
        historical_data["Value"].rename("Historical values"),
        create_series(predictions_ml, "Predicted values ML"),
        create_series(predictions_baseline, "Predicted values Baseline")
    ], axis=1)

    return predictions_dataset
```

## 전체 코드
다음의 Python 코드는 `algorithms/algorithms.py` 파일에 해당한다. 모든 함수를 함께 연쇄하면 다음과 같은 그래프로 나타낼 수 있다.

![](./images/config_toml.png)

```python
# For the sake of clarity, we have used an AutoRegressive model rather than a pure ML model such as:
# Random Forest, Linear Regression, LSTM, etc   
from statsmodels.tsa.ar_model import AutoReg
import pandas as pd
from sklearn.metrics import mean_squared_error, mean_absolute_error
import numpy as np
import datetime as dt

def clean_data(initial_dataset: pd.DataFrame):
    print("     Cleaning data")
    # Convert the date column to datetime
    initial_dataset['Date'] = pd.to_datetime(initial_dataset['Date'])
    cleaned_dataset = initial_dataset.copy()
    return cleaned_dataset


def predict_baseline(cleaned_dataset: pd.DataFrame, n_predictions: int, day: dt.datetime, max_capacity: int):
    print("     Predicting baseline")
    # Select the train data
    train_dataset = cleaned_dataset[cleaned_dataset['Date'] < day]

    predictions = train_dataset['Value'][-n_predictions:].reset_index(drop=True)
    predictions = predictions.apply(lambda x: min(x, max_capacity))
    return predictions


# This is the function that will be used by the task
def predict_ml(cleaned_dataset: pd.DataFrame, n_predictions: int, day: dt.datetime, max_capacity: int):
    print("     Predicting with ML")
    # Select the train data
    train_dataset = cleaned_dataset[cleaned_dataset["Date"] < day]

    # Fit the AutoRegressive model
    model = AutoReg(train_dataset["Value"], lags=7).fit()

    # Get the n_predictions forecasts
    predictions = model.forecast(n_predictions).reset_index(drop=True)
    predictions = predictions.apply(lambda x: min(x, max_capacity))
    return predictions


def compute_metrics(historical_data, predicted_data):
    historical_to_compare = historical_data[-len(predicted_data):]['Value']
    rmse = mean_squared_error(historical_to_compare, predicted_data)
    mae = mean_absolute_error(historical_to_compare, predicted_data)
    return rmse, mae


def create_predictions_dataset(predictions_baseline, predictions_ml, day, n_predictions, cleaned_data):
    print("Creating predictions dataset...")

    # Create the historical dataset that will be displayed
    new_length = len(cleaned_data[cleaned_data["Date"] < day]) + n_predictions
    historical_data = cleaned_data[:new_length].reset_index(drop=True)

    create_series = lambda data, name: pd.Series([np.NaN] * (len(historical_data)), name=name).fillna({i: val for i, val in enumerate(data, len(historical_data)-n_predictions)})

    predictions_dataset = pd.concat([
        historical_data["Date"],
        historical_data["Value"].rename("Historical values"),
        create_series(predictions_ml, "Predicted values ML"),
        create_series(predictions_baseline, "Predicted values Baseline")
    ], axis=1)

    return predictions_dataset
```
