# 성능 페이지

> 전체 코드는 [여기](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/complete_application/src/src.zip)에서 다운로드받을 수 있다.

성능 페이지는 사용자가 다양한 시나리오에서 루트 평균 제곱 오차(Root Mean Squared Error)와 평균 절대 오차(Mean Absolute Error,)를 포함한 성능 메트릭을 비교할 수 있는 어플리케이션의 섹션이다. 이 페이지에는 기준선과 기계 학습 예측 간의 이러한 메트릭을 비교하기 위한 표와 두 개의 막대 차트가 표시된다.

![](./images/result_03.png)

## 마크다운
다음 마크다운 코드는 `page/performance/performance.md` 파일에 해당한다.

```md
<|part|render={len(comparison_scenario)>0}|

<|Table|expanded=False|expandable|
<|{comparison_scenario}|table|>
|>

<|{selected_metric}|selector|lov={metric_selector}|dropdown|>

<|{comparison_scenario}|chart|type=bar|x=Scenario Name|y[1]=RMSE baseline|y[2]=RMSE ML|render={selected_metric=="RMSE"}|>

<|{comparison_scenario}|chart|type=bar|x=Scenario Name|y[1]=MAE baseline|y[2]=MAE ML|render={selected_metric=="MAE"}|>
|>


<center><|Compare primarys|button|on_action=compare|></center>
```

Markdown 섹션에서는 성는 페이지의 레이아웃과 구성 요소를 정의한다. 여기에는 다음 요소를 포함한다.

- **Conditional Rendering**: `<|part|render={len(comparison_scenario)>0}|...|>`<br>
    부품 구성 요소는 `comparison_scenario`에서 사용할 수 있는 데이터가 있을 때만 특정 요소가 렌더링되도록 한다.
- **Table**: `<|{comparison_scenario}|table|>`<br>
    `comparison_scenario` DataFrame의 비교 데이터를 표시하는 테이블이다. 행 수가 사용 가능한 공간을 초과하면 테이블을 확장할 수 있다.
- **Metric Selector**: `<|{selected_metric}|selector|lov={metric_selector}|dropdown|>`<br>
    RMSE(Root Mean Squared Error)와 MAE( Mean Absolute Error)의 두 메트릭 중에서 선택할 수 있는 드롭다운 선택기이다.
- **Bar Charts**: `<|{comparison_scenario}|차트|type=bar|x=scenario Name|y[1]=RMSE baseline|y[2]=RMSE ML|...|>`<br>
    기준선과 기계 학습 예측 간에 선택한 메트릭(RMSE 또는 MAE)을 비교하는 두 개의 막대 차트. 차트에는 각 시나리오의 성능 메트릭이 표시된다.
- **Compare Button**: `<|Compare primarys|button|on_action=compare|>`<br>
    클릭 시 "compare" 작업을 트리거하는 버튼으로, 비교 프로세스를 시작하는 데 사용된다.

## Python 코드
다음 Python 코드는 `pages/performance/performance.py` 파일에 해당한다.

```python
from taipy.gui import Markdown

import pandas as pd
import taipy as tp

# Initial dataset for comparison
comparison_scenario = pd.DataFrame(columns=["Scenario Name",
                                            "RMSE baseline",
                                            "MAE baseline",
                                            "RMSE ML",
                                            "MAE ML"])


# Selector for metrics
metric_selector = ["RMSE", "MAE"]
selected_metric = metric_selector[0]


def compare(state):
    print("Comparing...")

    # Initialize lists for comparison
    scenario_data = []

    # Go through all the primary scenarios
    all_scenarios = sorted(tp.get_primary_scenarios(), key=lambda x: x.creation_date.timestamp())

    for scenario in all_scenarios:
        rmse_baseline, mae_baseline = scenario.metrics_baseline.read()
        rmse_ml, mae_ml = scenario.metrics_ml.read()


        # Store scenario data in a dictionary
        scenario_data.append({
            "Scenario Name": scenario.name,
            "RMSE baseline": rmse_baseline,
            "MAE baseline": mae_baseline,
            "RMSE ML": rmse_ml,
            "MAE ML": mae_ml
        })

    # Create a DataFrame from the scenario_data list
    state.comparison_scenario = pd.DataFrame(scenario_data)

performance = Markdown("pages/performance/performance.md")
```

- **Global Variables**:<br>
    변수 comparison_scenario, metric_selector 및 selected_metric은 초기화됩니다. metric_selector는 메트릭 선택기의 옵션을 유지하는 반면, comparison_scenario DataFrame은 비교 데이터를 저장합니다. selected_metric은 기본 메트릭으로 초기화되며, 이 메트릭은 metric_selector list(RMSE)의 첫 번째 요소입니다.
- **Compare Function**:<br>
    비교 함수는 비교 프로세스를 처리한다. 이 함수는 사용자가 "compare" 버튼을 클릭하면 트리거된다. 어플리케이션에서 기본 시나리오를 수집한 다음 각 시나리오를 통해 기준선과 기계 학습 예측에 대한 RMSE와 MAE 메트릭을 수집한다.

그런 다음 데이터는 `comparison_scenario` DataFrame에 저장된다.

Python 어플리케이션의 성능 페이지를 통해 시계열 예측 시 서로 다른 시나리오의 효과를 비교할 수 있다. 사용자는 RMSE 메트릭과 MAE 메트릭 중 하나를 선택하여 막대 차트로 표시된 비교 결과를 볼 수 있다. 이 페이지는 다양한 예측 시나리오의 효율성을 평가하는 데 유용한 도구이며 성능 평가를 기반으로 정보에 입각한 의사 결정을 내리는 데 도움을 줄 수 있다.
