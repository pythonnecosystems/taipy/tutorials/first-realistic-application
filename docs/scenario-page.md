# 시나리오 페이지

> 전체 코드는 [여기](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/complete_application/src/src.zip)에서 다운로드받을 수 있다.

시나리오 페이지는 시계열 데이터를 사용하여 예측 시나리오를 만들고 커스터마이징하기 위해 설계된 어플리케이션의 일부이다. 최종 사용자는 예측 날짜, 최대 용량 및 예측 수 같은 예측을 위한 다양한 매개 변수를 수정할 수 있다. 이 페이지에는 기계 학습과 기준 방법을 모두 사용하여 생성된 과거 데이터와 예측을 표시하는 차트도 포함된다.

![](./images/result_02.png)

시나리오 페이지는 마크다운과 Python 코드의 조합으로 구성되며, 아래에서 자세히 설명한다.

## 마크다운
다음 마크다운 코드는 `page/scenario/scenario.md` 파일에 해당한다.

```md
# Create your scenario:

<|layout|columns=3 1 1 1 1|
<|{scenario}|scenario_selector|>

**Prediction date** <br/>
<|{day}|date|active={scenario}|not with_time|>

**Max capacity** <br/>
<|{max_capacity}|number|active={scenario}|>

**Number of predictions** <br/>
<|{n_predictions}|number|active={scenario}|>

<br/> <|Save|button|on_action=save|active={scenario}|>
|>

<|{scenario}|scenario|on_submission_change=submission_change|>

<|{predictions_dataset}|chart|x=Date|y[1]=Historical values|type[1]=bar|y[2]=Predicted values ML|y[3]=Predicted values Baseline|>

# Data Node Exploration

<|layout|columns=1 5|
<|{data_node}|data_node_selector|>

<|{data_node}|data_node|>
|>
```

Markdown 섹션에서는 시나리오 페이지의 배열과 요소에 대해 개략적으로 설명한다. 이 페이지는 다음과 같은 구성 요소로 이루어진다.

- **Scenario Selector**: `<|{scenario}|scenario_selector|>`<br>
    사용자가 서로 다른 시나리오를 선택할 수 있는 구성요소(드롭다운)이다. 선택된 시나리오는 페이지의 다른 구성요소의 값에 영향을 미친다. 시나리오 값은 다른 요소에서 사용되므로 시나리오를 선택하면 다른 요소에 영향을 미친다.
- **Prediction Date**: `<|{day}|date|...|>`<br>
    사용자가 예측할 날짜를 선택할 수 있는 날짜 선택기이다. 선택한 날짜는 기계 학습과 기준 예측에 모두 사용된다.
- **Max Capacity**: `<|{max_capacity}|number|...|>`<br>
    사용자가 최대 용량 값을 설정할 수 있는 숫자 입력 필드이다. 이 값은 지정된 최대값을 초과하는 경우 예측값을 제한하는 데 사용된다.
- **Number of Predictions**: `<|{n_predictions}|number|...|>`<br>
    사용자가 원하는 예측 수를 설정할 수 있는 숫자 입력 필드이다.
- **Save Button**: <|Save|button|on_action=save|active={scenario}|><br>
    클릭 시 "저장" 작업을 트리거하는 버튼으로, 선택한 시나리오와 파라미터 값을 저장하는 데 사용된다.
- **Scenario Section**: `<|{scenario}|scenario|on_submission_change=submission_change|>`<br>
    현재 선택한 시나리오에 대한 정보를 표시하는 섹션이다. 시나리오, 속성, 시나리오 삭제 또는 제출 기능에 대한 세부 정보를 포함한다. 시나리오 상태가 변경되면 `submission_change`가 시나리오에 대한 정보와 함께 호출된다.
- **Predictions Chart**: `<|{predictions_dataset}|chart|...|>`<br>
    기계 학습과 기준 방법에서 얻은 과거 값과 예측 값을 표시하는 차트이다. 예측이 과거 데이터와 얼마나 잘 일치하는지 보여준다.
- **Data Node Exploration**: `<|{data_node}|data_node_selector|> <|{data_node}|data_node|>`<br>
    여기서 선택한 데이터 노드에 대한 자세한 정보와 이력이 제시된다. 이는 데이터 노드의 특성에 따라 원시 데이터를 표 형식, 시각화, 텍스트 또는 날짜로 표시할 수 있다. 형식이 허용하면 사용자가 데이터 노드에 직접 새로운 값을 쓸수 수도 있다.

## Python 코드
다음 Python 코드는` pages/scenario/scenario.py` 파일에 해당하며 시나리오 페이지의 상태를 초기화하여 관리한다.

```python
from taipy.gui import Markdown, notify
import datetime as dt
import pandas as pd


scenario = None
data_node = None
day = dt.datetime(2021, 7, 26)
n_predictions = 40
max_capacity = 200
predictions_dataset = {"Date":[0], 
                       "Predicted values ML":[0],
                       "Predicted values Baseline":[0],
                       "Historical values":[0]}

def submission_change(state, submittable, details: dict):
    if details['submission_status'] == 'COMPLETED':
        notify(state, "success", 'Scenario completed!')
        state['scenario'].on_change(state, 'scenario', state.scenario)
    else:
        notify(state, "error", 'Something went wrong!')

def save(state):
    print("Saving scenario...")
    # Get the currently selected scenario

    # Conversion to the right format
    state_day = dt.datetime(state.day.year, state.day.month, state.day.day)

    # Change the default parameters by writing in the Data Nodes
    state.scenario.day.write(state_day)
    state.scenario.n_predictions.write(int(state.n_predictions))
    state.scenario.max_capacity.write(int(state.max_capacity))
    notify(state, "success", "Saved!")


def on_change(state, var_name, var_value):
    if var_name == "scenario" and var_value:
        state.day = state.scenario.day.read()
        state.n_predictions = state.scenario.n_predictions.read()
        state.max_capacity = state.scenario.max_capacity.read()

        if state.scenario.full_predictions.is_ready_for_reading:
            state.predictions_dataset = state.scenario.full_predictions.read()
        else:
            state.predictions_dataset = predictions_dataset



scenario_page = Markdown("pages/scenario/scenario.md")
```

여기에는 다음 구성 요소를 포함하고 있다.

- **Global Variables**:<br>
    전역 변수 `senario`, `data_node`, `day`, `n_predictions`, `max_capacity` 및 `predictions_dataset`를 정의한다. 이 변수들은 어플리케이션의 초기 상태를 저장한다.
- **Save Function**:<br>
    저장 기능은 콜백(callback)으로 사용됩니다. 현재 시나리오 상태를 유지하는 기능을 담당하며, 현재 시나리오 상태를 유지하는 기능을 담당합니다. 사용자가 "저장" 버튼을 클릭하면 이 기능이 활성화됩니다. 페이지의 상태를 입력으로 받아 날짜 형식을 올바른 것으로 변환하고, 그에 따라 시나리오 파라미터를 조정한 후 성공 메시지와 함께 사용자에게 알려줍니다.
- **Submission Status Change**:<br>
    `submission_change` 함수는 시나리오 제출이 완료되었을 때 피드백을 처리할 수 있도록 설계되었다. 현재 상태, 제출된 객체와 제출에 대한 세부 정보를 포함한다. 제출이 성공하면(`'COMPLETED'`) 사용자에게 성공 알림을 보내고 시나리오 객체에 대한 업데이트를 트리거한다. 실패 시 오류 알림을 제공하여 사용자에게 문제가 있음을 알린다.
- **On Change Function**:<br>
    `on_change` 함수는 페이지의 어떤 변수가 값 변화가 일어날 때마다 호출된다. 그것은 시나리오 변수의 변화를 추적하고 그에 따라 다른 변수들을 조정한다. 그것은 또한 `full_predictions`이 읽을 수 있는지 확인하고 그에 따라 `predictions_dataset`을 업데이트한다.
- **Scenario Page Initialization**:<br>
    `scenario_page` 변수는 시나리오 페이지의 내용을 나타내는 Markdown 객체로 초기화된다.

사용자가 시계열 예측을 위한 다양한 시나리오를 만들고 커스터마이즈할 수 있는 대화형 인터페이스를 제공한다. 사용자가 예측 날짜를 선택하고, 최대 용량을 설정하고, 예측할 수 있는 수를 선택할 수 있도록 한다. 이 페이지는 또한 기계 학습과 기준 방법 모두에서 과거 데이터와 예측 값을 시각화할 수 있는 차트를 제공한다. 사용자는 추가 분석과 비교를 위해 선택한 시나리오를 저장할 수 있다.

## 전체 어플리케이션과 연결
`scenario` 페이지에서 생성된 `on_change` 함수를 사용한다. 어플리케이션의 전역 `on_change(메인 스크립트)`에서 호출해야 한다. 이 전역 함수는 사용자 인터페이스에서 변수가 변경될 때마다 호출된다.

메인 스크립트에서:

```python
def on_change(state, var_name: str, var_value):
    state['scenario'].on_change(state, var_name, var_value)
```
