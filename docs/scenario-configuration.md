# 시나리오 구성

> 전체 코드는 [여기](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/complete_application/src/src.zip)에서 다운로드받을 수 있다.

## 시나리오
Taipy는 효율적으로 다음을 수행할 수 있는 간편한 시나리오 관리 기능을 제공한다.

- 함수/파이프라인의 실행을 관리한다
- 데이터 소스를 관리하고 KPI를 모니터링한다

기계 학습 또는 수학적 최적화의 맥락에서 유용하다.

시나리오가 무엇인지 이해하려면 데이터 노드와 작업 개념을 이해해야 한다.

- **데이터 노드(Data Node)**: Taipy에서 변수를 변환한 것이다. 데이터 노드는 데이터 자체를 포함하지 않고 데이터를 가리키며 검색하는 방법을 알고 있다. 이러한 데이터 노드는 CSV 파일, Pickle 파일, 데이터베이스 등 다양한 유형의 데이터 소스를 가리키며 정수, 문자열, 데이터 프레임, 리스트 등 다양한 유형의 Python 변수를 나타낼 수 있다.
- **작업(Task)**: Taipy에서 함수의 변환은 입력과 출력이 데이터 노드이다.
- **시나리오(Senario)**: 데이터 노드와 작업을 결합하여 실행 흐름을 매핑하는 그래프를 구성하여 시나리오를 만든다. 최종 사용자는 종종 다양한 비즈니스 상황을 반영하기 위한 다양한 파라미터 수정 기능이 필요하다. Taipy는 다양한 상황(즉, 최종 사용자가 설정한 다양한 데이터/파라미터 값)에서 시나리오를 재생/실행할 수 있는 프레임워크를 제공한다.

## 구성 기본사항
시나리오를 만들고 실행하기 전에 시나리오를 정학히 구성해야 한다.

### 데이터 노드 구성
Data Node 구성 중, 개발자는 각 Data Node의 유형 또는 형식과 그 범위를 지정한다.

- Storage Type: CSV 파일, Pickle 파일 등과 같은 Data Node의 스토리지 타입을 지정한다. 예를 들어 초기 데이터세트는 storage_type="csv"인 CSV 파일이다.
- Scope: Data Node의 범위를 정의한다. 코드의 scope에는 Global, Cycle, Scenario 세 가지 종류가 있다.
    - `Scope.Scenario` (기본값): 각 시나리오에 하나의 데이터 노드를 갖는다.
    - `Scope.CYCLE`: 주어진 주기의 모든 시나리오에 걸쳐 데이터 노드를 공유하며 범위를 확장한다.
    - `Scope.GLOBAL`: 마지막으로 (모든 사이클의 모든 시나리오에 걸쳐) 전역적으로 범위를 확장한다. 예를 들어, 초기/이력 데이터세트는 일반적으로 모든 시나리오/사이클에 의해 공유된다. 전체 어플리케이션에서 고유하다.

기계 학습 상황에서는 일반적으로 여러 개의 훈련과 테스트 모델이 있다. 이 튜토리얼에서는 기준 모델과 머신 러닝 모델의 두 가지 모델을 사용하여 특정 날짜를 기준으로 다가오는 며칠 동안의 값을 예측하는 시나리오를 설정한다.

- 초기 데이터세트 검색
- 데이터 정리
- 예측(예측 횟수에 대한) **day** 이후의 예측. 이 예제에서 예측은 15분 단위로 주어진 상점에서 판매되는 품목의 수를 나타낸다
- 메트릭 생성과 시각화를 위한 데이터세트 생성

아래 그래프는 구성할 시나리오를 나타내며, 여기서 태스크를 주황색으로, 데이터 노드를 파란색으로 표시하였다.

![](./images/config_toml_02.png)

#### 입력 데이터 노드 구성
이들은 입력 데이터 노드이다. 시나리오가 실행될 때 Taipy의 변수/데이터 소스를 나타낸다. 그러나 처음에는 DAG를 구축하기 위해 설정해야 한다.

`initial_dataset`은 단순히 초기 CSV 파일이다. Taipy는 이 데이터를 읽는 데 경로와 헤더라는 몇 가지 매개 변수가 필요하다. 범위는 글로벌이고 각 시나리오는 동일한 초기 데이터세트를 갖는다.

`day`는 예측의 시작이다. 기본값은 7월 26일이다. 이것은 훈련 데이터는 7월 26일 이전 이고, 예측은 이날부터 시작된다는 것을 의미한다.

`n_predictions`는 예측하는 동안 수행할 예측의 수를 나타낸다. 기본값은 `40`이다. 예측은 15분 단위로 주어진 상점에서 판매되는 품목들의 수를 나타낸다.

`max_capacity`는 예측값이 취할 수 있는 최대값이며, 예측값의 상한값이다. 기본값은 `200`이다. 즉, 15분당 판매되는 품목의 최대 값은 `200`이다.

```python
import datetime as dt
import pandas as pd

from taipy import Config, Scope

## Input Data Nodes
initial_dataset_cfg = Config.configure_data_node(id="initial_dataset",
                                                 storage_type="csv",
                                                 path=path_to_csv,
                                                 scope=Scope.GLOBAL)

# We assume the current day is the 26th of July 2021.
# This day can be changed to simulate multiple executions of scenarios on different days
day_cfg = Config.configure_data_node(id="day", default_data=dt.datetime(2021, 7, 26))

n_predictions_cfg = Config.configure_data_node(id="n_predictions", default_data=40)

max_capacity_cfg = Config.configure_data_node(id="max_capacity", default_data=200)
```

#### 중간 및 출력 데이터 노드

- `clean_dataset`는 `clean_data` 함수 이후의 정리된 데이터세트이다.
- `prediction_ml`과 `prediction_baseline`은 예측 모델이다. `prediction_baseline()`과 `prediction_ml()` 함수의 출력이다.
- `full_dataset`는 예측을 연결한 것이다.
- 데이터 노드 `metrics`는 예측에 대한 KPI를 저장한다.

```python
## Remaining Data Nodes
cleaned_dataset_cfg = Config.configure_data_node(id="cleaned_dataset",
                                                 scope=Scope.GLOBAL) 

predictions_baseline_cfg = Config.configure_data_node(id="predictions_baseline")
predictions_ml_cfg = Config.configure_data_node(id="predictions_ml")

full_predictions_cfg = Config.configure_data_node(id="full_predictions")

metrics_baseline_cfg = Config.configure_data_node(id="metrics_baseline")
metrics_ml_cfg = Config.configure_data_node(id="metrics_ml")
```

### 작업 구성
작업은 Taipy의 함수를 번역하는 것이다. 각 작업은 ID, 함수, 입력 및 출력이 있다.

#### clean_data 작업
우리가 만들고 싶은 첫 번째 작업은 clean_data 작업이다. 초기 데이터세트(입력 데이터 노드)가 필요하고, 그것을 정리하고(`clean_data` 함수라고 부르며), 정리된 데이터세트 Data Node를 생성한다. 이 작업은 Taipy의 skipability 기능 덕분에 한 번만 실행된다.

![](./images/clean_data.svg)

```python
clean_data_task_cfg = Config.configure_task(id="clean_data",
                                            function=clean_data,
                                            input=initial_dataset_cfg,
                                            output=cleaned_dataset_cfg,
                                            skippable=True)
```

#### predict_baseline 작업
이 작업에서는 정리된 데이터세트를 사용하여 지정된 매개 변수인 3개의 입력 데이터 노드를 기반으로 예측을 수행한다.

Day, Number of predictions 및 Max Capacity.

![](./images/predict_baseline.svg)

```python
predict_baseline_task_cfg = Config.configure_task(id="predict_baseline",
                                                  function=predict_baseline,
                                                  input=[cleaned_dataset_cfg, n_predictions_cfg, day_cfg, max_capacity_cfg],
                                                  output=predictions_cfg)
```

다른 작업(`predict_ml`, `metrics_baseline`, `metrics_ml` 및 `full_prediction`)은 두 모델과 모든 예측 및 과거 데이터가 포함된 데이터 세트에서 메트릭을 가져오는 동일한 방법으로 구성된다.

### 시나리오 구성
모든 작업과 Data Node 구성으로부터 시나리오를 생성할 수 있다. 실행 그래프를 구성하는 이러한 작업은 시나리오가 제출되면 실행된다.

```python
scenario_cfg = Config.configure_scenario(id="scenario",
                                         task_configs=[clean_data_task_cfg,
                                                       predict_baseline_task_cfg,
                                                       predict_ml_task_cfg,
                                                       metrics_baseline_task_cfg,
                                                       metrics_ml_task_cfg,
                                                       full_predictions_task_cfg],
                                         frequency=Frequency.WEEKLY)
```

## 전체 코드
다음 Python 코드는 `configuration/config.py` 파일에 해당한다.

```python
import datetime as dt
import pandas as pd

from taipy import Config, Scope, Frequency

from algos.algos import *

path_to_csv = "data/dataset.csv"

# Datanodes
## Input Data Nodes
initial_dataset_cfg = Config.configure_data_node(id="initial_dataset",
                                                 storage_type="csv",
                                                 path=path_to_csv,
                                                 scope=Scope.GLOBAL)

# We assume the current day is the 26th of July 2021.
# This day can be changed to simulate multiple executions of scenarios on different days
day_cfg = Config.configure_data_node(id="day", default_data=dt.datetime(2021, 7, 26))

n_predictions_cfg = Config.configure_data_node(id="n_predictions", default_data=40)

max_capacity_cfg = Config.configure_data_node(id="max_capacity", default_data=200)

## Remaining Data Nodes
cleaned_dataset_cfg = Config.configure_data_node(id="cleaned_dataset",
                                                 scope=Scope.GLOBAL)

predictions_baseline_cfg = Config.configure_data_node(id="predictions_baseline")
predictions_ml_cfg = Config.configure_data_node(id="predictions_ml")

full_predictions_cfg = Config.configure_data_node(id="full_predictions")

metrics_baseline_cfg = Config.configure_data_node(id="metrics_baseline")
metrics_ml_cfg = Config.configure_data_node(id="metrics_ml")

# Tasks
clean_data_task_cfg = Config.configure_task(id="task_clean_data",
                                            function=clean_data,
                                            input=initial_dataset_cfg,
                                            output=cleaned_dataset_cfg,
                                            skippable=True)


predict_baseline_task_cfg = Config.configure_task(id="predict_baseline",
                                                  function=predict_baseline,
                                                  input=[cleaned_dataset_cfg, n_predictions_cfg, day_cfg,
                                                         max_capacity_cfg],
                                                  output=predictions_baseline_cfg)

predict_ml_task_cfg = Config.configure_task(id="task_predict_ml",
                                            function=predict_ml,
                                            input=[cleaned_dataset_cfg,
                                                   n_predictions_cfg, day_cfg,
                                                   max_capacity_cfg],
                                            output=predictions_ml_cfg)


metrics_baseline_task_cfg = Config.configure_task(id="task_metrics_baseline",
                                            function=compute_metrics,
                                            input=[cleaned_dataset_cfg,
                                                   predictions_baseline_cfg],
                                            output=metrics_baseline_cfg)

metrics_ml_task_cfg = Config.configure_task(id="task_metrics_ml",
                                            function=compute_metrics,
                                            input=[cleaned_dataset_cfg,
                                                   predictions_ml_cfg],
                                            output=metrics_ml_cfg)


full_predictions_task_cfg = Config.configure_task(id="task_full_predictions",
                                            function=create_predictions_dataset,
                                            input=[predictions_baseline_cfg,
                                                   predictions_ml_cfg,
                                                  day_cfg,
                                                  n_predictions_cfg,
                                                  cleaned_dataset_cfg],
                                            output=full_predictions_cfg)


# Configure our scenario which is our business problem.
scenario_cfg = Config.configure_scenario(id="scenario",
                                         task_configs=[clean_data_task_cfg,
                                                       predict_baseline_task_cfg,
                                                       predict_ml_task_cfg,
                                                       metrics_baseline_task_cfg,
                                                       metrics_ml_task_cfg,
                                                       full_predictions_task_cfg],
                                          frequency=Frequency.WEEKLY)
```
