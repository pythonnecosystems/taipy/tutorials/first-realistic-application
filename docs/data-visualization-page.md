# 데이터 시각화 페이지

> 전체 코드는 [여기](https://docs.taipy.io/en/release-3.0/knowledge_base/tutorials/complete_application/src/src.zip)에서 다운로드받을 수 있다.

예를 들어 Data Visualization 페이지를 만들기 위한 가이드이다. 페이지에는 CSV 파일의 데이터를 보여주기 위한 대화형 시각적 요소가 포함되어 있다.

![](./images/result.gif)

## 데이터세트 임포트
데이터세트를 임초트라려면 다음 Python 코드를 실행한다.

```python
import pandas as pd

def get_data(path_to_csv: str):
    dataset = pd.read_csv(path_to_csv)
    dataset["Date"] = pd.to_datetime(dataset["Date"])
    return dataset

path_to_csv = "dataset.csv"
dataset = get_data(path_to_csv)
```

Taipy는 클라이언트 인터페이스에 보여지는 그래픽 객체인 *시각적 요소(Visual element)*의 개념을 소개한다. [slider](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/slider/), [chart](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/chart/), [table](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/table/), [input](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/input/), [menu](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/menu/) 등 다양한 시각적 요소를 사용할 수 있다. [여기](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/)에서 목록을 볼 수 있다. 시각적 요소를 추가하기 위한 구문은 다음과 같다.

```
<|{variable}|visual_element_name|param_1=param_1|param_2=param_2| ... |>
```

예를 들어 변수 `n_week`의 값을 수정하는 [slider](https://docs.taipy.io/en/release-3.0/manuals/gui/viselements/slider/)를 추가하려면 다음 구문을 사용한다.

```
<|{n_week}|slider|min=1|max=52|>
```

데이터세트의 내용으로 chart를 디스플레이하려면 다음 구문을 사용한다.

```
<|{dataset}|chart|type=bar|x=Date|y=Value|>
```

## 대화형 GUI
Data Visualization(데이터 시각화) 페이지에는 다음과 같은 시각적 요소을 포함한다.

- Python 변수 `n_week`에 연결된 slider
- DataFrame 콘텐츠를 나타내는 chart

## 다중 클라이언트 - state
Taipy는 모든 클라이언트 연결에 대해 고유한 state를 유지한다. 이 state는 사용자 인터페이스에 사용되는 모든 변수의 값을 저장한다. 예를 들어, slider를 통해 `n_week`을 수정하면 글로벌 Python 변수 `n_week`이 아니라 `state.n_week`이 업데이트된다. 각 클라이언트는 고유한 state를 가지며, 한 클라이언트가 변경한 내용이 다른 클라이언트에 영향을 미치지 않도록 보장한다.

## 콜백(Callback)
대부분의 시각적 요소에는 [callback](https://docs.taipy.io/en/release-3.0/manuals/gui/callbacks/)이 포함되어 사용자 행동에 따라 변수를 수정할 수 있다. 자세한 내용은 로컬 콜백과 글로벌 콜백을 참고하세요.

- `state`: 모든 변수를 포함하는 `state` 객체
- 수정된 변수의 이름 (선택사항)
- 새로운 값 (선택사항)

다음은 slider에서 선택한 주를 기준으로 `state.dataset_week`를 업데이트하기 위하여 `on_change` 콜백 함수를 설정하는 예이다.

```
<|{n_week}|slider|min=1|max=52|on_change=on_slider|>
```

```python
def on_slider(state):
    state.dataset_week = dataset[dataset["Date"].dt.isocalendar().week == state.n_week]
```

## 마크다운(Markdown)
아래 마크다운은 `pages/data_viz/data_viz.md` 파일의 내용이다. 첫 페이지의 전체 마크다운이다.

```md
# Data Visualization page

Select week: *<|{n_week}|>*

<|{n_week}|slider|min=1|max=52|on_change=on_slider|>

<|{dataset_week}|chart|type=bar|x=Date|y=Value|>
```

## Python 코드 (`pages/data_viz/data_viz.py`)
아래 Python 코드는 `pages/data_viz/data_viz.py` 파일에 해당한다. 위의 마크다운을 보완하는 코드이다. 이 코드는 페이지의 객체들을 채우고 slider와 chart 사이의 연결을 만든다.

```python
from taipy.gui import Markdown
import pandas as pd

def get_data(path_to_csv: str):
    # pandas.read_csv() returns a pd.DataFrame
    dataset = pd.read_csv(path_to_csv)
    dataset["Date"] = pd.to_datetime(dataset["Date"])
    return dataset

# Read the dataframe
path_to_csv = "data/dataset.csv"
dataset = get_data(path_to_csv)

# Initial value
n_week = 10

# Select the week based on the slider value
dataset_week = dataset[dataset["Date"].dt.isocalendar().week == n_week]

def on_slider(state):
    state.dataset_week = dataset[dataset["Date"].dt.isocalendar().week == state.n_week]


data_viz = Markdown("pages/data_viz/data_viz.md")
```

이 설정에 따라 Taipy를 사용하여 대화형 데이터 시각화 페이지를 구성할 수 있다. 이 페이지에서는 slider에서 선택한 주에 해당하는 데이터세트를 보여준다.
