# 첫 실감형 어플리케이션

> Taipy은 Python 3.8 이상이 필요하다.

이 튜토리얼은 프론트 엔드에서 백 엔드로 전체 어플리케이션을 만드는 과정을 안내한다. 이 튜토리얼을 완료하는 데 사전 지식이 필요없다.

![](./images/overview.gif)

각 단계는 Taipy에 대한 기본적인 아이디어에 초점을 맞추었다.

## 어플리케이션의 목적
데이터 시각화, 예측 분석 및 비교 평가를 위해 설계된 포괄적인 멀티 페이지 어플리케이션을 만들려고 한다. 이 앱은 판매 수치를 처리하여 표시한다. 전용 페이지 중 하나를 사용하면 예측을 일부 매개 변수를 통해 미세 조정할 수 있는 두 개의 예측 모델을 사용할 수 있다. 앱을 마무리하기 위하여 성능 페이지는 다양한 예측 결과를 그래픽으로 비교한다.

## 시작 전

1. **Taipy** 패키지, Python 3.8 이상이 필요하다
1. **scikit-learn**: 튜토라얼 사용자 코드에 사용될 기계 학습 패키지
1. **statsmodels**: 사용자 코드에도 사용되는 통계를 위한 또 다른 패키지

```python
$ pip install taipy
$ pip install scikit-learn
$ pip install statsmodels
```

> **Info**
>
> `pip install taipy`는 최신 안정 버전의 Taipy를 설치하기 위해 선호되는 방법이다.
>
> [`pip`](https://pip.pypa.io/)이 설치되어 있지 않은 경우 이 [Taipy 설치 가이드](https://docs.taipy.io/en/release-3.0/installation/)를 통해 프로세스를 안내할 수 있다.

Taipy가 설치되면 CLI를 사용하여 어플리케이션 폴더를 스캐폴딩할 수 있다. 기본 템플릿으로 create 명령을 실행하고 다음과 같이 기본 질문에 답한다.

```python
> taipy create --template default
Application root folder name [taipy_application]:
Application main Python file [main.py]:
Application title [Taipy Application]:
Page names in multi-page application? []: data_viz scenario performance
Does the application use scenario management or version management? [No]: yes
Does the application use Rest API? [No]: no
```

그럼 더 이상 지체하지 말고 코딩을 시작하겠다!

## Steps

1. [데이터 시각화 페이지](data-visualization-page.md)
1. [적용된 알고리즘](algorithm-used.md)
1. [시나리오 구성](scenario-configuration.md)
1. [시나리오 페이지](scenario-page.md)
1. [성능 페이지](performance-page.md)
